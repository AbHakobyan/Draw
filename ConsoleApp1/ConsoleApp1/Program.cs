﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Draw
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Tringle");
            var tringle = new Tringle
            {
                Sym = '$',
                Size = 5
            };
            tringle.Draw();


            Console.WriteLine();
            Console.WriteLine("Rectrangl \n");
            var rectangle = new Rectangle()
            {
                Sym = '*', Size = 5
            };

            rectangle.Draw();

            Console.WriteLine();
            Console.WriteLine("Line \n");

            var line = new Line()
            {
                Sym='&',Size = 15
            };
            line.Draw();

            Console.WriteLine("\n Square \n");
            var square = new Square()
            {
                Sym = '%', Size = 9
            };
            square.Draw();

            Console.WriteLine("\n   Arrow");
            var arrow = new Arrow()
            {
                Sym = '*', Size = 5
            };

            arrow.Draw();

            Console.WriteLine("\n   Dooble Arrow");
            var d_arrow = new Dooble_Arrow()
            {
                Sym = '_' , Size = 10
            };

            d_arrow.Draw();

        }
    }
}
